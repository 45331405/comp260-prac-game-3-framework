﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitpoint : MonoBehaviour {

    public TargetMove target;

	// Use this for initialization
	void Start () {
        target = GetComponentInParent<TargetMove>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    private void OnCollisionEnter(Collision collision)
    {
        print("ouch");
        target.hit = true;
    }
}
