﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

    public float speed = 10.0f;
    public Vector3 direction;
    private Rigidbody rigidbody;

    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        rigidbody.velocity = speed * direction;
    }

    private void OnCollisionEnter(Collision collision)
    {
        print("hit" + collision.gameObject.name);
        Destroy(gameObject, 0.01f);
    }

}
