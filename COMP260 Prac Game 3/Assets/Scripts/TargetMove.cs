﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour {

    public float startTime = 0.0f;
    private Animator animator;
    public bool hit;
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        // set the Start parameter to true
        // if we have passed the start time
        if (Time.time >= startTime)
        {
            animator.SetTrigger("Start");
        }


        if (hit == true)
        {
            animator.SetTrigger("Hit");
        }
    }

   

    void Destroy()
    {
        Destroy(gameObject);
    }
}
